import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Scanner;


public class NetInterfacesParser {

    public static NetworkInterface getInterface() throws SocketException {
        List<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());

        for (NetworkInterface netint : networkInterfaces) {
            if (netint.isUp()) {
                displayInterfaceInformation(netint);
            }
        }

        Scanner in = new Scanner(System.in);

        System.out.print("Enter interface name: ");
        String intName = in.nextLine();
        NetworkInterface networkInterface = NetworkInterface.getByName(intName);

        while (networkInterface == null) {
            System.out.print("Enter interface name: ");
            intName = in.nextLine();
            networkInterface = NetworkInterface.getByName(intName);
        }

        return networkInterface;
    }

    private static void displayInterfaceInformation(NetworkInterface netint) {
        System.out.printf("Name: %s\n", netint.getName());
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
            System.out.printf("InetAddress: %s\n", inetAddress);
        }
        System.out.print("\n");
    }
}  