import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CopyFinder implements Runnable {
    private final MulticastSocket sender = new MulticastSocket();
    private final MulticastSocket receiver = new MulticastSocket(5000);
    private final Map<SocketAddress, Long> addressMap = new HashMap<>();
    private final List<NetworkInterface> interfaces = new ArrayList<>();

    private final String MESSAGE = "PING";
    private final int MSG_DELAY = 2500;

    public CopyFinder(String groupAddress) throws IOException {

        // NetworkInterface networkInterface = NetInterfacesParser.getInterface();
        InetAddress groupAddr = InetAddress.getByName(groupAddress);

        NetworkInterface.networkInterfaces().forEach(i -> {
            try {
                if (i.isUp() && !i.isLoopback() && i.supportsMulticast()) {
                    System.out.println(i);
                    interfaces.add(i);
                    receiver.joinGroup(new InetSocketAddress(groupAddr, receiver.getLocalPort()), i);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        int port = receiver.getLocalPort();

        sender.connect(groupAddr, port);
    }

    private void ping() {
        DatagramPacket data = new DatagramPacket(MESSAGE.getBytes(), MESSAGE.length());
        while (Thread.currentThread().isAlive()) {
            try {

                interfaces.forEach(i -> {
                    try {
                        sender.setNetworkInterface(i);
                        sender.send(data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                Thread.sleep(MSG_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private boolean filter(SocketAddress address) {

        boolean changed = false;

        Long currentTime = System.currentTimeMillis();

        if (addressMap.containsKey(address)) {
            addressMap.replace(address, currentTime);
        } else {
            addressMap.put(address, currentTime);
            changed = true;
        }

        changed |= addressMap.values().removeIf(time -> (currentTime - time > 2 * MSG_DELAY));

        return changed;
    }


    private void scan() {
        while (Thread.currentThread().isAlive()) {
            byte[] recvBuf = new byte[MESSAGE.length()];

            DatagramPacket data = new DatagramPacket(recvBuf, MESSAGE.length());

            try {
                receiver.receive(data);
            } catch (IOException e) {
                e.printStackTrace();
            }


            if (filter(data.getSocketAddress())) {
                System.out.println("\nCopyList: ");
                addressMap.forEach((addr, time) -> System.out.println(addr.toString()));
            }

        }
    }

    @Override
    public void run() {
        Thread sender = new Thread(this::ping);
        Thread recv = new Thread(this::scan);

        sender.start();
        recv.start();
    }
}
