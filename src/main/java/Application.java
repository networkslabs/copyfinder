import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        new CopyFinder(args[0]).run();
    }
}
